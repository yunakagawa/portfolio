require 'rails_helper'

RSpec.feature 'Top', type: :feature do
  let!(:product) { create(:product) }
  let!(:new_product) { create(:product) }

  background do
    visit potepan_path
  end

  scenario 'User visits top page' do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario 'Product details are displayed on the new product page' do
    within ".featuredProducts" do
      expect(page).to have_link new_product.name, href: potepan_product_path(id: new_product.id)
      expect(page).to have_link new_product.display_price, href: potepan_product_path(id: new_product.id)
    end
  end
end
