require 'rails_helper'

RSpec.feature 'Products', type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  background do
    visit potepan_product_path(id: product.id)
  end

  scenario 'User visits products page' do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario 'Product details are displayed on the related product page' do
    within ".productsContent" do
      expect(page).to have_link related_product.name, href: potepan_product_path(id: related_product.id)
      expect(page).to have_link related_product.display_price, href: potepan_product_path(id: related_product.id)
    end
  end
end
