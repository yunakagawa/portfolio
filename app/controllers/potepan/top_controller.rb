class Potepan::TopController < ApplicationController
  MAX_NUMBER_OF_NEW_PRODUCTS = 8
  def index
    @products = Spree::Product.all.includes(master: [:default_price, :images])
    @new_products = @products.order(available_on: "DESC").limit(MAX_NUMBER_OF_NEW_PRODUCTS)
  end
end
